# breakout-game

Inspired from the original arcade game, Breakout.

I developed this from scratch and I didn't base the code from anywhere so I'm not sure if my code is optimized or correct but it was fun coding this.

## Controls
Use mouse to move the platform.

Demo: https://projects.juvarabrera.com/breakout-game
