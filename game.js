class BreakoutGame {
    static CONFIG = {
        "MAX_ALLOWED_BRICKS_PER_ROW": 10,
        "MAX_BRICK_ROW": 5,
        "MAX_BRICKS": 30
    }
    constructor(container) {
        this.container = container;
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.container.classList.add("game");
        this.start();
        this.track();
    }
    track() {
        let that = this;
        this.container.onmousemove = function(event) {
            // move paddle first
            that.paddle.x = event.clientX - (that.paddle.width / 2);
            if(that.paddle.x < 0) {
                that.paddle.x = 0;
            } else if(that.paddle.x + that.paddle.width > that.width) {
                that.paddle.x = that.width - that.paddle.width;
            }
            that.paddle.element.style.left = that.paddle.x + "px";

            // move ball
            if(that.ball.status == Ball.STATUS.REST) {
                that.ball.x = that.paddle.x + (that.paddle.width / 2) - (that.ball.diameter / 2);
                that.ball.element.style.left = that.ball.x + "px";
            }
        }
        document.onkeydown = function(event) {
            if(event.key == " ") {
                that.ball.launch();
            }
        }
    }
    start() {
        this.paddle = new Paddle(this);
        this.paddle.spawn();
        this.ball = new Ball(this);
        this.ball.spawn();
        this.bricks = [];
        for(let i = 0; i < BreakoutGame.CONFIG.MAX_BRICKS; i++) {
            this.bricks.push(new Brick(this));
            this.bricks[i].spawn();
        }
    }
}
class Brick {
    static SIDES = {
        "TOP": 1,
        "RIGHT": 2,
        "BOTTOM": 3,
        "LEFT": 4
    }
    static SIDES_STRING = [
        "", "TOP", "RIGHT", "BOTTOM", "LEFT"
    ];
    static CONFIG = {
        MAX_COLORS: 8
    }
    constructor(game) {
        this.element = document.createElement("div");
        this.game = game;
        this.x = null;
        this.y = null;
        this.status = true;
        this.width = this.game.width / (BreakoutGame.CONFIG.MAX_ALLOWED_BRICKS_PER_ROW + 2);
        this.height = this.game.ball.diameter;
    }
    checkIfOverlapped() {
        for(let i = 0; i < this.game.bricks.length - 1; i++)
            if(this.x == this.game.bricks[i].x &&
                this.y == this.game.bricks[i].y)
                return true;
        return false;
    }
    spawn() {
        let color = Math.floor(Math.random() * Brick.CONFIG.MAX_COLORS) + 1
        this.element.classList.add("brick", "brick-" + color);
        while(this.x == null || this.checkIfOverlapped()) {
            this.x = this.width + ((Math.floor(Math.random() * BreakoutGame.CONFIG.MAX_ALLOWED_BRICKS_PER_ROW)) * this.width);
            this.y = this.game.height - this.game.paddle.bottomSpacing - ((Math.floor(Math.random() * BreakoutGame.CONFIG.MAX_BRICK_ROW)) * this.height);
        }
        this.element.style.width = this.width + "px";
        this.element.style.height = this.height + "px";
        this.element.style.left = this.x + "px";
        this.element.style.bottom = this.y + "px";
        this.game.container.insertAdjacentElement("beforeend", this.element);
    }
    remove() {
        this.status = false;
        this.element.remove();
    }
}
class Ball {
    static STATUS = {
        "REST": 0,
        "MOVE": 1
    }
    static DEFAULT_ANGLES = {
        "LAUNCH": {
            "MINIMUM": 45,
            "MAXIMUM": 135
        },
        "MOVE": {
            "UP": {
                "MINIMUM": 10,
                "MIDDLE": 90,
                "MAXIMUM": 170,
            },
            "DOWN": {
                "MINIMUM": 190,
                "MIDDLE": 270,
                "MAXIMUM": 350,
            }
        }
    }
    static DIRECTION = {
        "UP": 0,
        "DOWN": 1,
        "LEFT": 2,
        "RIGHT": 3,
        "NEITHER": 4
    }
    static DIRECTION_STRING = [
        "UP", "DOWN", "LEFT", "RIGHT", "NEITHER"
    ]
    constructor(game) {
        this.element = document.createElement("div");
        this.game = game;
        this.status = Ball.STATUS.REST;
        this.diameter = 25;
        this.speed = 2;
        this.angle = 0;
        this.x = 0;
        this.y = 0;
    }
    spawn() {
        this.element.classList.add("ball");
        this.element.style.width = this.diameter + "px";
        this.element.style.height = this.diameter + "px";
        this.x = this.game.paddle.x + (this.game.paddle.width / 2) - (this.diameter / 2);
        this.y = this.game.paddle.bottomSpacing + this.game.paddle.height;
        this.element.style.left = this.x + "px";
        this.element.style.bottom = this.y + "px";
        this.game.container.insertAdjacentElement("beforeend", this.element);
    }
    getDirectionY() {
        if(this.angle >= Ball.DEFAULT_ANGLES.MOVE.UP.MINIMUM &&
            this.angle <= Ball.DEFAULT_ANGLES.MOVE.UP.MAXIMUM)
                return Ball.DIRECTION.UP;
        return Ball.DIRECTION.DOWN;
    }
    getDirectionX() {
        if(this.angle > Ball.DEFAULT_ANGLES.MOVE.UP.MIDDLE &&
            this.angle < Ball.DEFAULT_ANGLES.MOVE.DOWN.MIDDLE)
            return Ball.DIRECTION.LEFT;
        else if(this.angle < Ball.DEFAULT_ANGLES.MOVE.UP.MIDDLE ||
            this.angle > Ball.DEFAULT_ANGLES.MOVE.DOWN.MIDDLE)
            return Ball.DIRECTION.RIGHT;
        return Ball.DIRECTION.NEITHER;
    }
    launch() {
        if(this.status != Ball.STATUS.REST) return;
        this.angle = Ball.DEFAULT_ANGLES.LAUNCH.MINIMUM + 
            Math.floor(Math.random() * 
                (Ball.DEFAULT_ANGLES.LAUNCH.MAXIMUM - 
                    Ball.DEFAULT_ANGLES.LAUNCH.MINIMUM)
            );
        this.angle = 135;
        this.status = Ball.STATUS.MOVE;
        this.move();
    }
    getVerticalMiddlePoint() {
        return this.x + (this.diameter / 2)
    }
    move() {
        let that = this;
        this.interval = setInterval(function() {
            let base = Math.sin((180 - 90 - that.angle) * Math.PI / 180) * that.speed;
            let height = Math.sin(that.angle * Math.PI / 180) * that.speed;
            that.y += height;
            that.x += base;
            that.element.style.left = that.x + "px";
            that.element.style.bottom = that.y + "px";

            // ball hits the side of the screen
            if(that.x <= 0 || that.x >= that.game.width - that.diameter) {
                // if ball is going up
                if(that.getDirectionY() == Ball.DIRECTION.UP) {
                    that.angle = 180 - that.angle;
                } else if(that.getDirectionX() == Ball.DIRECTION.LEFT) {
                    that.angle = 360 - that.angle - 180;
                } else if(that.getDirectionX() == Ball.DIRECTION.RIGHT) {
                    that.angle = 360 - that.angle + 180;
                }
                if(that.x < 0)
                    that.x = 0;
                else if(that.x > that.game.width - that.diameter)
                    that.x = that.game.width - that.diameter;
            }
            // if ball touches the top side of the screen
            if(that.y + that.diameter >= that.game.height) {
                that.angle = 360 - that.angle;
                that.y = that.game.height - that.diameter;
            }

            let bx1 = that.x;
            let bx2 = bx1 + that.diameter;
            let px1 = that.game.paddle.x;
            let px2 = px1 + that.game.paddle.width;

            let by1 = that.y + that.diameter;
            let by2 = that.y;
            let py1 = that.game.paddle.bottomSpacing + that.game.paddle.height;
            let py2 = that.game.paddle.bottomSpacing;

            // if ball touches the left side of the paddle
            if(bx2 >= px1 && bx1 < px1 && (
                (by1 <= py1 && by1 >= py2) || 
                (by2 <= py1 && by2 >= py2)
            ) && that.getDirectionX() == Ball.DIRECTION.RIGHT) {
                that.angle -= 90;
            } else if(bx1 <= px2 && bx2 > px2 && (
                (by1 <= py1 && by1 >= py2) || 
                (by2 <= py1 && by2 >= py2)
            ) && that.getDirectionX() == Ball.DIRECTION.LEFT) {
                that.angle += 90;
            } else if(((bx1 >= px1 && bx1 <= px2) || (bx2 >= px1 && bx2 <= px2)) &&
                (that.y <= that.game.paddle.bottomSpacing + that.game.paddle.height) && 
                (that.y >= that.game.paddle.bottomSpacing) && 
                that.getDirectionY() == Ball.DIRECTION.DOWN) {
                let pointTouched = (that.getVerticalMiddlePoint() - px1) / that.game.paddle.width;
                if(pointTouched < 0) pointTouched = 0;
                else if (pointTouched > 1) pointTouched = 1;
                that.angle = 180 - ((pointTouched * (Ball.DEFAULT_ANGLES.MOVE.UP.MAXIMUM - Ball.DEFAULT_ANGLES.MOVE.UP.MINIMUM)) + Ball.DEFAULT_ANGLES.MOVE.UP.MINIMUM);
            }

            for(let i = 0; i < that.game.bricks.length; i++) {
                let sideCollided = that.checkIfCollidedWithBrick(that.game.bricks[i]);
                if(sideCollided != null) {
                    console.log("Ball collided on side: " + Brick.SIDES_STRING[sideCollided])
                    console.log("Angle before collision: " + that.angle + " [" + Ball.DIRECTION_STRING[that.getDirectionX()] + " " + Ball.DIRECTION_STRING[that.getDirectionY()] + "]");
                    that.game.bricks[i].remove();
                    if(sideCollided == Brick.SIDES.TOP && that.getDirectionY() == Ball.DIRECTION.DOWN) {
                        // that.angle = 180 - that.angle;
                        that.angle = 360 - that.angle;
                        console.log("A");
                    } else if((sideCollided == Brick.SIDES.RIGHT && that.getDirectionY() == Ball.DIRECTION.LEFT) || 
                        (sideCollided == Brick.SIDES.LEFT && that.getDirectionY() == Ball.DIRECTION.RIGHT)) {
                        if(that.getDirectionY() == Ball.DIRECTION.UP) {
                            that.angle = 180 - that.angle;
                            console.log("B");
                        } else if(that.getDirectionX() == Ball.DIRECTION.LEFT) {
                            that.angle = 360 - that.angle - 180;
                            console.log("C");
                        } else if(that.getDirectionX() == Ball.DIRECTION.RIGHT) {
                            that.angle = 360 - that.angle + 180;
                            console.log("D");
                        }
                    } else if(sideCollided == Brick.SIDES.BOTTOM && that.getDirectionY() == Ball.DIRECTION.UP) {
                        // that.angle = 180 - that.angle;
                        that.angle = 270 - (that.angle - 90);
                        // if(that.getDirectionX() == Ball.DIRECTION.LEFT) {
                        // } else if(that.getDirectionX() == Ball.DIRECTION.RIGHT)
                        console.log("E");
                    }
                    console.log("Angle after collision: " + that.angle + " [" + Ball.DIRECTION_STRING[that.getDirectionX()] + " " + Ball.DIRECTION_STRING[that.getDirectionY()] + "]");
                    console.log("------------")
                }
            }
        });
    }
    checkIfCollidedWithBrick(brick) {
        if(!brick.status) return null;

        let bx1 = this.x;
        let bx2 = bx1 + this.diameter;
        let by1 = this.y + this.diameter;
        let by2 = this.y;

        let kx1 = brick.x;
        let kx2 = kx1 + brick.width;
        let ky1 = brick.y + brick.height;
        let ky2 = brick.y;

        if(((bx1 >= kx1 && bx1 <= kx2) || (bx2 >= kx1 && bx2 <= kx2)) &&
            by2 <= ky1 && by1 > ky1) {
            return Brick.SIDES.TOP;
        } else if(((by1 <= ky1 && by1 >= ky2) || (by2 <= ky1 && by2 >= ky2)) &&
            bx1 <= kx2 && bx2 > kx2) {
            return Brick.SIDES.RIGHT;
        } else if(((bx1 >= kx1 && bx1 <= kx2) || (bx2 >= kx1 && bx2 <= kx2)) &&
            by1 >= ky2 && by2 < ky2) {
            return Brick.SIDES.BOTTOM;
        } else if(((by1 <= ky1 && by1 >= ky2) || (by2 <= ky1 && by2 >= ky2)) &&
            bx2 >= kx1 && bx1 < kx1) {
            return Brick.SIDES.LEFT;
        }
        return null;
    }
}
class Paddle {
    constructor(game) {
        this.element = document.createElement("div");
        this.game = game;
        this.width = 100;
        this.height = 25;
        this.bottomSpacing = 50;
        this.x = 0;
    }
    spawn() {
        this.element.classList.add("paddle");
        this.element.style.width = this.width + "px";
        this.element.style.height = this.height + "px";
        this.element.style.bottom = this.bottomSpacing + "px";
        this.x = (this.game.width / 2) - (this.width / 2);
        this.element.style.left = this.x + "px";
        this.game.container.insertAdjacentElement("beforeend", this.element);
    }
}